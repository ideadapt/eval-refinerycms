module Refinery
  module References
    class Reference < Refinery::Core::BaseModel
      self.table_name = 'refinery_references'

      attr_accessible :title, :description, :image1_id, :image2_id, :image3_id, :image4_id, :position

      acts_as_indexed :fields => [:title, :description]

      validates :title, :presence => true, :uniqueness => true

      belongs_to :image1, :class_name => '::Refinery::Image'

      belongs_to :image2, :class_name => '::Refinery::Image'

      belongs_to :image3, :class_name => '::Refinery::Image'

      belongs_to :image4, :class_name => '::Refinery::Image'
    end
  end
end
