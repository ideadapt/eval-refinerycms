class AddSliderImagesToRefineryPages < ActiveRecord::Migration
  def change
    add_column :refinery_pages, :slider_image1_id, :integer
    add_column :refinery_pages, :slider_image2_id, :integer
    add_column :refinery_pages, :slider_image3_id, :integer
    add_column :refinery_pages, :slider_image4_id, :integer
  end
end
